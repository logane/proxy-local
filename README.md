# First run

## Generate self-signed certificates

Use this command:

```bash
openssl req -nodes -new -x509 -keyout certs/cert.key -out certs/cert.crt
```

## Add .env file

Duplicate example:

```bash
cp .env.example .env
```

## (optional) Update dashboard url

Set dashboard url by updating `TRAEFIK_HTTP_ROUTERS_API_RULE` variable.

By default, the dashboard url is set to <https://traefik.localhost>. Feel free to update it if you need.

## Run proxy

Use this command:

```bash
docker-compose up -d
```

## Adding domain

In project `docker-compose.yml` file, add this section in container:

```yml
version: "3"

services:
    # ... previous containers ...
    web:
        labels:
        - "traefik.enable=true"
        - "traefik.docker.network=webproxy"
        
        - "traefik.http.routers.web-${COMPOSE_NAME}.rule=Host(`your-domain.localhost`)"
        - "traefik.http.routers.web-${COMPOSE_NAME}.entrypoints=websecure"
        - "traefik.http.routers.web-${COMPOSE_NAME}.tls=true"
        networks:
        - webproxy
    # ... next containers ...
```

And this network:

```yml
networks:
  webproxy:
    external: true
```
